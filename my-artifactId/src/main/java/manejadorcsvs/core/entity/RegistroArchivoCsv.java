package manejadorcsvs.core.entity;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class RegistroArchivoCsv {
    private Integer idRow;
    private String registro;
}
