package manejadorcsvs.core.business.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.ibm.asyncutil.util.Either;
import lombok.extern.slf4j.Slf4j;
import manejadorcsvs.core.business.input.ManejadorArchivosService;
import manejadorcsvs.core.business.output.ManejadorArchivoRepository;
import manejadorcsvs.core.entity.RegistroArchivoCsv;
import manejadorcsvs.external.json.schemas.ArchivoCsv;
import manejadorcsvs.external.json.schemas.EjemploJson;
import manejadorcsvs.external.json.schemas.Employee;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@ApplicationScoped
public class ManejadorArchivosBs implements ManejadorArchivosService {
    @Inject
    ManejadorArchivoRepository manejadorArchivoRepository;

    @Override
    @Transactional
    public Boolean importarArchivo(MultipartFormDataInput inputFile) {
        try {
            log.info("Se esta ejectuando el body");
            CsvMapper csvMapper = CsvMapper.builder().build();
            CsvSchema csvSchema = CsvSchema.builder().setUseHeader(true).build().withColumnSeparator('|');
            List<InputPart> inputParts = inputFile.getFormDataMap().get("file");
            InputStream inputStream = inputParts.get(0).getBody(InputStream.class, null);
            List<? extends Object> rows = csvMapper.readerFor(ArchivoCsv.class).with(csvSchema).readValues(
                    new InputStreamReader(inputStream, StandardCharsets.UTF_8)).readAll();
            var filas = (List<ArchivoCsv>) rows;

            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            AtomicInteger row = new AtomicInteger(1);
            filas.forEach(fila -> {
                try {
                    manejadorArchivoRepository.saveRowCsv(RegistroArchivoCsv.builder()
                            .idRow(row.getAndIncrement())
                            .registro(ow.writeValueAsString(fila))
                            .build());
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            });

            log.info("Imprimir contenido del JSON:");
            ObjectMapper mapper = new ObjectMapper();
            filas.forEach(fila -> {
                try {
                    EjemploJson employee = mapper.readValue(fila.getJsonData(), EjemploJson.class);
                    log.info("El salario del empleado es: {}", employee.getEmployee().getSalary());
                } catch (JsonProcessingException e) {
                    log.error(e.toString());
                }
            });

        } catch (Exception ex) {
            log.error(ex.toString());
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
