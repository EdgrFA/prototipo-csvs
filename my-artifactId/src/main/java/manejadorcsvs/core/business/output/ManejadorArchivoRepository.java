package manejadorcsvs.core.business.output;

import manejadorcsvs.core.entity.RegistroArchivoCsv;

public interface ManejadorArchivoRepository {

    Integer saveRowCsv(RegistroArchivoCsv registro);
}
