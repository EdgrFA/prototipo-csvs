package manejadorcsvs.core.business.input;

import com.ibm.asyncutil.util.Either;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

public interface ManejadorArchivosService {

    Boolean importarArchivo(MultipartFormDataInput inputFile);
}
