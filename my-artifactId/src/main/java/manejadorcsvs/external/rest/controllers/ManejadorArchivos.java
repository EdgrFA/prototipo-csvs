package manejadorcsvs.external.rest.controllers;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import manejadorcsvs.core.business.input.ManejadorArchivosService;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

@Path("/manejador-archivos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Manejador Archivos", description = "Servicio para gestión de tablas de parámetros adicionales")
public class ManejadorArchivos {
    @Inject
    ManejadorArchivosService manejadorArchivosService;

    @POST
    @Path("importar-csv")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @APIResponse(responseCode = "200", description = "Operación exitosa")
    @Operation(operationId = "importarArchivoCsv",
            summary = "Importa un archivo csv", description = "Importa un archivo csv.")
    public Boolean importarArchivoCsv(@MultipartForm MultipartFormDataInput inputFile) {
        return manejadorArchivosService.importarArchivo(inputFile);
    }
}
