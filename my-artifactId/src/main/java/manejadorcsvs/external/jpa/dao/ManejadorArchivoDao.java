package manejadorcsvs.external.jpa.dao;

import lombok.extern.slf4j.Slf4j;
import manejadorcsvs.core.business.output.ManejadorArchivoRepository;
import manejadorcsvs.core.entity.RegistroArchivoCsv;

import javax.enterprise.context.ApplicationScoped;

@Slf4j
@ApplicationScoped
public class ManejadorArchivoDao implements ManejadorArchivoRepository {

    @Override
    public Integer saveRowCsv(RegistroArchivoCsv registro) {
        log.info("Se registro el objeto con identificador: {}, con contenido: {}",
                registro.getIdRow(), registro.getRegistro());
        return registro.getIdRow();
    }
}
