package manejadorcsvs.external.json.schemas;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EjemploJson {
    @JsonProperty(value = "employee", required = true)
    Employee employee;
}
