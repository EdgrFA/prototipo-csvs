package manejadorcsvs.external.json.schemas;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonPropertyOrder({"name", "salary", "married"})
public class Employee {
    @JsonProperty(value = "name", required = true)
    private String name;
    @JsonProperty(value = "salary", required = true)
    private Integer salary;
    @JsonProperty(value = "married", required = true)
    private Boolean married;
}
