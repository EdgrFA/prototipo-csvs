package manejadorcsvs.external.json.schemas;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"CVE_INST","INSTITU","NOMCORTO","NOMCOREP","EMP_INS","ENT","LET_INST","CVE_INS_O","JSON_DATA"})
public class ArchivoCsv {
    @JsonProperty(value = "CVE_INST", required = true)
    private String cveInst;
    @JsonProperty(value = "INSTITU")
    private String institu;
    @JsonProperty(value = "NOMCORTO")
    private String nomcorto;
    @JsonProperty(value = "NOMCOREP")
    private String nomcorep;
    @JsonProperty(value = "EMP_INS")
    private String empIns;
    @JsonProperty(value = "ENT")
    private String cveEnt;
    @JsonProperty(value = "LET_INST")
    private String letInst;
    @JsonProperty(value = "CVE_INS_O")
    private String cveInsO;
    @JsonProperty(value = "JSON_DATA")
    private String jsonData;
}
